/****************************************************************************
 *	Instituto Brasileiro de Tecnologia Avan�ada - 2016 S2					*
 *  Computa��o Gr�fica - Prof� Dr� S�rgio Penedo							*
 *																			*
 *  Renata Silva															*
 *  Luiz S�																	*
 *                                                                          *
 *	Lista 1 - Exerc�cio 4													*
 *  																		*
 * 	Aplicar filtro passa baixo e passa alta utilizando m�scaras de filtra- 	*
 *	gem arbitr�rias															*
 *                                                                          *
 *	Instru��es:																*
 *	- Todas as imagens de entrada tem de ser .BMP 24bpp						*																			*
 *																			*
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "../common/image_processing.h"
#include "../common/helpers.h"
#include "../common/file_bitmap.h"

/*
 * @brief Serve apenas para printar no terminal o sucesso ou n�o do carregamento da imagem
 * 
 * @param file_success_status 		Estado de sucesso do carregamento de arquivo
*/
void bitmap_file_success_printf( int32_t file_success_status );

const int32_t conv_mask_highpass[3][3] = { {0,1,0},{1,-4,1},{0,1,0} };
const int32_t conv_mask_lowpass[3][3] = { {1,2,1},{2,5,2},{1,2,1} };

/**
 * @brief Loop principal
 *
 * Loop principal do programa
 */
int main( /*int argc, char *argv[] */) {
	int32_t file_a_success = 0;
	
	bitmap_t bitmap_input;
	bitmap_t bitmap_lowpass;
	bitmap_t bitmap_hipass;

	/*	DEFINE OS ARQUIVOS DE ENTRADA */
	file_a_success = bitmap_ReadFromFile( &bitmap_input , "../bitmaps/baboon.bmp" );
	
	/* Verifica se o arquivo de entrada da imagem de refer�ncia � v�lido*/
	printf("Arquivo Original :\n");
	bitmap_file_success_printf( file_a_success );
	
	printf("\nAguarde ");
	
	/* Caso as duas imagens sejam v�lidas*/
	if( file_a_success ==  BITMAP_READFROMFILE_SUCCESS ){
		
		/* Applica o passa Altas */
		processing_ApplyConvolutionMask( &bitmap_hipass , &bitmap_input , &conv_mask_highpass[0][0] );
		bitmap_SaveToFile( &bitmap_hipass, "output_hipass.bmp" );	
		printf(".");
		
		/* Aplica o passa Baixas */
		processing_ApplyConvolutionMask( &bitmap_lowpass , &bitmap_input , conv_mask_lowpass );
		bitmap_SaveToFile( &bitmap_lowpass, "output_lowpass.bmp" );		
		printf(".");
	}
		
		
	printf(" Fim!");
	/* Libera os bitmaps da mem�ria*/
	bitmap_FreeBitmap( &bitmap_input );
	bitmap_FreeBitmap( &bitmap_lowpass );
	bitmap_FreeBitmap( &bitmap_hipass );
	
	return 0;
}

void bitmap_file_success_printf( int32_t file_success_status ){
	switch( file_success_status ){
		case BITMAP_READFROMFILE_SUCCESS:
			printf("Sucesso!\n");
		break;
		
		case BITMAP_READFROMFILE_ERROR_FILENOTFOUND:
			printf("ERRO: Arquivo nao localizado!\n");
		break;
		
		case BITMAP_READFROMFILE_ERROR_NOTBITMAP:
			printf("ERRO: O arquivo fornecido nao e um BITMAP!\n");
		break;
		case BITMAP_READFROMFILE_ERROR_BITDEPTHNOTSUPPORTED:
			printf("ERRO: O BitDepth da imagem n�o � suportado!\n");
		break;
		default:
			printf("ERRO: ???\n");
		break;
	}
}
