/****************************************************************************
 *	Instituto Brasileiro de Tecnologia Avan�ada - 2016 S2					*
 *  Computa��o Gr�fica - Prof� Dr� S�rgio Penedo							*
 *																			*
 *  Renata Silva															*
 *  Luiz S�																	*
 *                                                                          *
 *	Lista 1 - Exerc�cio 2													*
 *  																		*
 * 	Transformar uma imagem RGB em uma imagem halftone						*
 *                                                                          *
 *	Instru��es:																*
 *	- Todas as imagens de entrada tem de ser .BMP 24bpp						*																			*
 *	- Para testar imagens diferentes basta substituir os endere�os na linha	*
 *	  67 e 68 deste arquivo													*
 *																			*
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "../common/image_processing.h"
#include "../common/helpers.h"
#include "../common/file_bitmap.h"

/*
 * @brief Serve apenas para printar no terminal o sucesso ou n�o do carregamento da imagem
 * 
 * @param file_success_status 		Estado de sucesso do carregamento de arquivo
*/
void bitmap_file_success_printf( int32_t file_success_status );

/**
 * @brief Loop principal
 *
 * Loop principal do programa
 */
int main( /*int argc, char *argv[] */) {
	int32_t file_a_success = 0;
	
	bitmap_t bitmap_input;
	bitmap_t bitmap_luma;
	bitmap_t bitmap_halftone;

	/*	DEFINE OS ARQUIVOS DE ENTRADA */
	file_a_success = bitmap_ReadFromFile( &bitmap_input , "../bitmaps/peppers.bmp" );
	
	/* Verifica se o arquivo de entrada da imagem de refer�ncia � v�lido*/
	printf("Arquivo Original :\n");
	bitmap_file_success_printf( file_a_success );
	
	printf("\nAguarde ");
	
	/* Caso as duas imagens sejam v�lidas*/
	if( file_a_success ==  BITMAP_READFROMFILE_SUCCESS ){
		/* Inicializa o bitmap de Luma */
		processing_RGBToLuma( &bitmap_luma , &bitmap_input );
		bitmap_SaveToFileIndexed( &bitmap_luma, "output_luma.bmp" , index_table_gray , sizeof( index_table_gray ) );
		printf(".");
		
		/* Normaliza��o para aumentar o alcance din�mico da imagem*/
		processing_NormalizeChannel( &bitmap_luma , 0.03 );
		bitmap_SaveToFileIndexed( &bitmap_luma, "output_normalized.bmp" , index_table_gray , sizeof( index_table_gray ) );
		printf(".");
		
		/* Gera o Halftone */
		processing_GrayToHalftone( &bitmap_halftone , &bitmap_luma );
		bitmap_SaveToFileIndexed( &bitmap_halftone, "output_halftone.bmp" , index_table_blackandwhite , sizeof( index_table_blackandwhite ) );		
		printf(".");
	}
		
		
	printf(" Fim!");
	/* Libera os bitmaps da mem�ria*/
	bitmap_FreeBitmap( &bitmap_input );
	bitmap_FreeBitmap( &bitmap_luma );
	bitmap_FreeBitmap( &bitmap_halftone );
	
	return 0;
}

void bitmap_file_success_printf( int32_t file_success_status ){
	switch( file_success_status ){
		case BITMAP_READFROMFILE_SUCCESS:
			printf("Sucesso!\n");
		break;
		
		case BITMAP_READFROMFILE_ERROR_FILENOTFOUND:
			printf("ERRO: Arquivo nao localizado!\n");
		break;
		
		case BITMAP_READFROMFILE_ERROR_NOTBITMAP:
			printf("ERRO: O arquivo fornecido nao e um BITMAP!\n");
		break;
		case BITMAP_READFROMFILE_ERROR_BITDEPTHNOTSUPPORTED:
			printf("ERRO: O BitDepth da imagem n�o � suportado!\n");
		break;
		default:
			printf("ERRO: ???\n");
		break;
	}
}
