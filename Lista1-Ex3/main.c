/****************************************************************************
 *	Instituto Brasileiro de Tecnologia Avan�ada - 2016 S2					*
 *  Computa��o Gr�fica - Prof� Dr� S�rgio Penedo							*
 *																			*
 *  Renata Silva															*
 *  Luiz S�																	*
 *                                                                          *
 *	Lista 1 - Exerc�cio 3													*
 *  																		*
 * 	Quantizar a tabela de cores de uma imagem 24bpp, para uma tabela de 	*
 *	128 cores																*
 *                                                                          *
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "../common/image_processing.h"
#include "../common/helpers.h"
#include "../common/file_bitmap.h"

/*
 * @brief Serve apenas para printar no terminal o sucesso ou n�o do carregamento da imagem
 * 
 * @param file_success_status 		Estado de sucesso do carregamento de arquivo
*/
void bitmap_file_success_printf( int32_t file_success_status );

void processing_QuantizeColorPallete( bitmap_t * bitmap , uint8_t color_pallete[][3] , int32_t colors ){
	
	int32_t c = 0;
	int32_t i = 0;
	int32_t j = 0;
	int32_t k = 0;
	
	uint8_t r = 0;
	uint8_t g = 0;
	uint8_t b = 0;
	
	uint32_t count = 0;
	
	uint8_t temp_color[3];
	
	uint32_t rgb_histogram[16][16][16];
	
	int32_t colors_count = colors/3;
	
	/* Reseta a tabela de cores*/
	for( i = 0 ; i < colors_count ; i++ ){
		color_pallete[i][RED] = 0;
		color_pallete[i][GRN] = 0;
		color_pallete[i][BLU] = 0;
	}
	
	/* Reseta os Histograma 3D */
	for( k = 0 ; k < 16 ; k++ ){
		for( j = 0 ; j < 16 ; j++ ){
			for( i = 0 ; i < 16; i++ ){
				rgb_histogram[k][j][i] = 0;
			} /* Blue */
		} /* Green */
	} /* Red */
	
	/* Popula o Histograma */
	for( j = 0 ; j < bitmap->height ; j++ ){
		for( i = 0 ; i < bitmap->width ; i++ ){
			r = ( bitmap->pixel[RED][j][i] >> 4 ) & 0xFF;
			g = ( bitmap->pixel[GRN][j][i] >> 4 ) & 0xFF;
			b = ( bitmap->pixel[BLU][j][i] >> 4 ) & 0xFF;
			
			rgb_histogram[r][g][b]++;
		} /* Eixo X */
	} /* Eixo Y */
	
	
	
	/*  */
	for( c = 0 ; c < colors_count ; c++ ){
		count = 0;
		
		for( k = 0 ; k < 16 ; k++ ){
			for( j = 0 ; j < 16 ; j++ ){
				for( i = 0 ; i < 16; i++ ){
					if( count < rgb_histogram[k][j][i] ){
						count = rgb_histogram[k][j][i];
						temp_color[RED] = k;
						temp_color[GRN] = j;
						temp_color[BLU] = i;
					}
				} /* Blue */
			} /* Green */
		} /* Red */
		
		color_pallete[c][RED] = ( temp_color[RED] << 4 ) & 0xFF;
		color_pallete[c][GRN] = ( temp_color[GRN] << 4 ) & 0xFF;
		color_pallete[c][BLU] = ( temp_color[BLU] << 4 ) & 0xFF;
		
		rgb_histogram[ temp_color[RED] ][ temp_color[GRN] ][ temp_color[BLU] ] = 0;
		
	} /* Tabela*/
	
}

/**
 * @brief Loop principal
 *
 * Loop principal do programa
 */
int main( /*int argc, char *argv[] */) {
	int32_t file_a_success = 0;
	
	bitmap_t bitmap_input;
	
	uint8_t index_table_colors[128][3];

	/*	DEFINE OS ARQUIVOS DE ENTRADA */
	
	printf("????:\n");
	
	file_a_success = bitmap_ReadFromFile( &bitmap_input , "../bitmaps/lenna.bmp" );
	
	/* Verifica se o arquivo de entrada da imagem de refer�ncia � v�lido*/
	printf("Arquivo Original :\n");
	bitmap_file_success_printf( file_a_success );
	
	printf("\nAguarde ");
	
	/* Caso as duas imagens sejam v�lidas*/
	if( file_a_success ==  BITMAP_READFROMFILE_SUCCESS ){
		
		/* Quantiza a Paleta de Cores baseado na imagem de entrada  */
		processing_QuantizeColorPallete( &bitmap_input , index_table_colors , sizeof( index_table_colors ) );
		
		/* Salva a imagem com 128 cores indexadas*/
		bitmap_SaveToFileIndexed( &bitmap_input, "output.bmp" , index_table_colors , sizeof( index_table_colors ) );
	}
		
		
	printf(" Fim!");
	/* Libera os bitmaps da mem�ria*/
	bitmap_FreeBitmap( &bitmap_input );
	
	return 0;
}

void bitmap_file_success_printf( int32_t file_success_status ){
	switch( file_success_status ){
		case BITMAP_READFROMFILE_SUCCESS:
			printf("Sucesso!\n");
		break;
		
		case BITMAP_READFROMFILE_ERROR_FILENOTFOUND:
			printf("ERRO: Arquivo nao localizado!\n");
		break;
		
		case BITMAP_READFROMFILE_ERROR_NOTBITMAP:
			printf("ERRO: O arquivo fornecido nao e um BITMAP!\n");
		break;
		case BITMAP_READFROMFILE_ERROR_BITDEPTHNOTSUPPORTED:
			printf("ERRO: O BitDepth da imagem n�o � suportado!\n");
		break;
		default:
			printf("ERRO: ???\n");
		break;
	}
}
