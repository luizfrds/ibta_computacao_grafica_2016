/****************************************************************************
 *	Copyright (C) 2016 by Luiz S�				                            *
 *                                                                          *
 *	Esse arquivo � parte das listas de Exerc�cio de Computa��o Gr�fica      *
 *	ministradas no 2� Semestre de 2016 pelo prof� S�rgio Penedo				*
 *                                                                          *
 ****************************************************************************/

/**
 * @file helpers.h
 * @author Luiz S�
 * @date 23 09 2016
 * @brief Fun��es de apoio
 *
 * -
 */

#ifndef _HELPERS_H
#define _HELPERS_H

/* Includes de sistema */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

/*
 * @brief retorna o valor m�nimo entre dois valores comparados
 * 
 * @param a		valor A
 * @param a		valor B
*/
double min( double a , double b );

/*
 * @brief retorna o valor m�ximo entre dois valores comparados
 * 
 * @param a		valor A
 * @param a		valor B
*/
double max( double a , double b );


#endif /* _HELPERS_H */





