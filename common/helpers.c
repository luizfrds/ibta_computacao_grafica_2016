/****************************************************************************
 *	Copyright (C) 2016 by Luiz S�				                            *
 *                                                                          *
 *	Esse arquivo � parte das listas de Exerc�cio de Computa��o Gr�fica      *
 *	ministradas no 2� Semestre de 2016 pelo prof� S�rgio Penedo				*
 *                                                                          *
 ****************************************************************************/

/**
 * @file helpers.c
 * @author Luiz S�
 * @date 23 09 2016
 * @brief Fun��es de apoio
 *
 * -
 */
 
#include "helpers.h"

double min( double a , double b ){
	return 	a < b ? a : b;
}

double max( double a , double b ){
	return 	a > b ? a : b;
}
