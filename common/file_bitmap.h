/****************************************************************************
 *	Copyright (C) 2016 by Luiz S�				                            *
 *                                                                          *
 *	Esse arquivo � parte das listas de Exerc�cio de Computa��o Gr�fica      *
 *	ministradas no 2� Semestre de 2016 pelo prof� S�rgio Penedo				*
 *                                                                          *
 ****************************************************************************/

/**
 * @file file_bitmap.h
 * @author Luiz S�
 * @date 10 09 2016
 * @brief Fun��es respons�veis por carregar arquivos bitmap e salv�-los no hdd
 *
 * As fun��es aqui s�o respons�veis por carregar e interpretar arquivos bin�rios 
 * de bitmap.
 * � Respons�vel tamb�m pela convers�o do objeto Bitmap gen�rico em BMP 
 * 
 * @see https://en.wikipedia.org/wiki/BMP_file_format
 */

#ifndef _FILE_BITMAP_H
#  define _FILE_BITMAP_H

/* Includes de sistema */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "image_processing.h"

/* Defini��es dos endere�os de cabe�alho do BMP file format */
#define BMP_OFFSET_HEADER									0x00
#define BMP_OFFSET_SIZE										0x02
#define BMP_OFFSET_PIXELSTART								0x0A
#define BMP_OFFSET_WIDTH									0x12
#define BMP_OFFSET_HEIGHT									0x16
#define BMP_OFFSET_BITSPERPIXEL								0x1C

#define BITMAP_READFROMFILE_SUCCESS							1
#define BITMAP_READFROMFILE_ERROR_NOTBITMAP					0
#define BITMAP_READFROMFILE_ERROR_FILENOTFOUND				-1
#define BITMAP_READFROMFILE_ERROR_BITDEPTHNOTSUPPORTED		-2

void	fput_u16( uint16_t value , FILE * fs );
void 	fput_u32( uint32_t value , FILE * fs );

/*
 * @brief Destr�i o objeto Bitmap
 * 
 * @param bitmap 	O objeto a ser destru�do
*/
void	bitmap_FreeBitmap( bitmap_t * bitmap  );

/*
 * @brief Inicializa o objeto de bitmap, alocando o espa�o necess�rio na mem�rio para guardar o arquivo.
 * 
 * @param bitmap 	O objeto a ser inicializado
*/
void	bitmap_InitBitmap( bitmap_t * bitmap , uint32_t width , uint32_t height , uint8_t channels );

/*
 * @brief L� um arquivo .bmp de um desino, e transforma para um "objeto" tBitmap
 * 
 * @param bitmap 	O apontador do objeto bitmap que acomodar� o novo bitmap
 * @param path		O caminho do arquivo .bmp
*/
int32_t	bitmap_ReadFromFile( bitmap_t * bitmap , const char * path );

/*
 * @brief Fun��o respons�vel por transformar o objeto de bitamp em arquivo de texto
 * 
 * @param bitmap 	O objeto a ser salvo 
 * @param path		O caminho do objeto
*/
void	bitmap_SaveToFile( bitmap_t * bitmap , const char * path );

/*
 * @brief Fun��o respons�vel por transformar o objeto de bitamp em arquivo de texto
 * 
 * @param bitmap 				O objeto a ser salvo 
 * @param path					O caminho do objeto
 * @param index_table[][3] 		A tabela de cores RGB
 * @param index_table_counter	O tamanho da tabela de cores
*/
void	bitmap_SaveToFileIndexed( bitmap_t * bitmap , const char * path , uint8_t index_table[][3] , uint32_t index_table_counter );


/*
 * @brief Respons�vel por copiar a informa��o de um bitmap para outra com ou sem offset
 * 
 * @param bitmap_dest		O bitmap de destino n�o inicializado onde ser� copiada o bitmap
 * @param bitmap_orig		O bitmap de origem
 * @param width				A largura do bitmap a ser copiado, caso seja para usar o valor do bitmap de origeml atribuir 0
 * @param height			A largura do bitmap a ser copiado, caso seja para usar o valor do bitmap de origeml atribuir 0
 * @param offset_x			O deslocamento em X, caso n�o haja deslocamento atribuir 0
 * @param offset_y			O deslocamento em Y, caso n�o haja deslocamento atribuir 0
*/
void	bitmap_Copy( bitmap_t * bitmap_dest , bitmap_t * bitmap_orig , uint32_t width , uint32_t height , uint32_t offset_x , uint32_t offset_y );

#endif /* _FILE_BITMAP_H */





