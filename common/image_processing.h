/****************************************************************************
 *	Copyright (C) 2016 by Luiz S�				                            *
 *                                                                          *
 *	Esse arquivo � parte das listas de Exerc�cio de Computa��o Gr�fica      *
 *	ministradas no 2� Semestre de 2016 pelo prof� S�rgio Penedo				*
 *                                                                          *
 ****************************************************************************/

/**
 * @file image_processing.h
 * @author Luiz S�
 * @date 23 09 2016
 * @brief Fun��es respons�veis por processamento de imagens
 *
 * Fun��es gen�ricas de processamento de imagens
 * 
 */

#ifndef _IMAGE_PROCESSING_H
#define _IMAGE_PROCESSING_H

/* Includes de sistema */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

extern uint8_t index_table_gray[256][3];
extern uint8_t index_table_blackandwhite[2][3];

typedef struct ConvMask{
	char		name[256];
	uint8_t		pixel[3][3];
} convmask_t;

/**
 * @brief Objeto Bitmap 8 bits por camada, n camadas
 *
 * Objeto de Bitmap composta de n camadas bidimensionais, cada pixel tendo 8 bits
 */
typedef struct Bitmaps{
	int32_t	width;
	int32_t	height;
	uint8_t	channels;
	uint8_t	***pixel;
} bitmap_t;

#define GRAY	0
#define RED		0
#define GRN		1
#define BLU		2

/*
 * @brief Calcula a dist�ncia euclidiana entre duas cores no padr�o RGB
 * 
 * @param r0 	Valor Vermelho da primera Cor
 * @param g0 	Valor Verde da primera Cor
 * @param b0 	Valor Azul da primera Cor 
 * @param r1 	Valor Vermelho da segunda Cor
 * @param g1 	Valor Verde da segunda Cor
 * @param b1 	Valor Azul da segunda Cor 
*/
double rgb_euclidean_distance( double r0, double g0, double b0, double r1, double g1, double b1 );

/*
 * @brief Desenha Ret�ngulo
 * 
 * @param bmp 			Bitmap onde o ret�ngulo ser� desenhado
 * @param border_width 	Largura da borda do ret�ngulo em pixels
 * @param x 			Offset vertical em rela��o ao canto superior esquerdo do bitmap
 * @param y 			Offset horizontal em rela��o ao canto superior esquerdo do bitmap
 * @param width			Largura do ret�ngulo
 * @param height		Altura do Ret�ngulo
*/
void draw_Rectangle( bitmap_t * bmp , uint32_t border_width , uint32_t x , uint32_t y , uint32_t width , uint32_t height );

/*
 * @brief Converte um bitmap RGB em Tons de Cinza
 * 
 * @param bmp_output 	Bitmap em Tons de Cinza
 * @param bmp_input 	Bitmap RGB em 3 camadas
*/
void processing_RGBToLuma( bitmap_t * bmp_output , bitmap_t * bmp_input );

/*
 * @brief Converte um bitmap em Tons de Cinza em bitmap Preto e Branco em Halftone
 * 
 * @param bmp_output 	Bitmap em Halftone 2 tons
 * @param bmp_input 	Imagem em tons de Cinza
*/
void processing_GrayToHalftone( bitmap_t * bmp_output , bitmap_t * bmp_input );

/*
 * @brief Executa o esticamento de histograma em uma imagem em tons de cinza. O valor percentual serve para definir
 * quanto das bordas minimas e m�ximas o histograma vai "comprimir".
 * 
 * @param bmp 			Bitmap em que ser� executado o esticamento de histograma
 * @param percentual 	Percentual de quantos pixels ser�o aglomerados nas extremidades do histograma
*/
void processing_NormalizeChannel( bitmap_t * bmp , double percentual );

/*
 * @brief Aplica uma m�scara de convolu��o em um bitmap atr�ves de uma m�scara 3x3 previamente definida;
 * 
 * @param bmp_output 	Bitmap de sa�da com a m�scara de convolu��o aplicada
 * @param bmp_input 	Bitmap de entrada
 * @param conv_mask 	M�scara 3x3 de convolu��o 
*/
void processing_ApplyConvolutionMask( bitmap_t * bmp_output , bitmap_t * bmp_input , const int32_t conv_mask[3][3] );

#endif /* _IMAGE_PROCESSING_H */





