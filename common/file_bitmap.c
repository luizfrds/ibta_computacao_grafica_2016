/****************************************************************************
 *	Copyright (C) 2016 by Luiz S�				                            *
 *                                                                          *
 *	Esse arquivo � parte das listas de Exerc�cio de Computa��o Gr�fica      *
 *	ministradas no 2� Semestre de 2016 pelo prof� S�rgio Penedo				*
 *                                                                          *
 ****************************************************************************/

/**
 * @file file_bitmap.c
 * @author Luiz S�
 * @date 10 09 2016
 * @brief Fun��es respons�veis por carregar arquivos bitmap e salv�-los no hdd
 *
 * As fun��es aqui s�o respons�veis por carregar e interpretar arquivos bin�rios 
 * de bitmap.
 * � Respons�vel tamb�m pela convers�o do objeto Bitmap gen�rico em BMP 
 * 
 * @see https://en.wikipedia.org/wiki/BMP_file_format
 */
 
#include "file_bitmap.h"


void fput_u32( uint32_t value , FILE * fs ){
	uint32_t i = 0;
	for( i = 0 ; i < 4 ; i++ ){
		fputc( value & 0xFF , fs );
		value >>= 8;
	}
}

void fput_u16( uint16_t value , FILE * fs ){
	uint32_t i = 0;
	for( i = 0 ; i < 2 ; i++ ){
		fputc( value & 0xFF , fs );
		value >>= 8;
	}
}

void bitmap_FreeBitmap( bitmap_t * bitmap  ){
	uint8_t i = 0;
	
	if( bitmap->width > 0 && bitmap->height > 0 ){
		for ( i = 0 ; i < bitmap->channels ; i++ ) {
	        free( bitmap->pixel[i] );
	    }
		
	    free( bitmap->pixel );
	}
}

void bitmap_InitBitmap( bitmap_t * bitmap , uint32_t width , uint32_t height , uint8_t channels ){	
    uint32_t i;
	uint32_t j;
	
	uint8_t * allElements;
    
	/* Define o tamanho do objeto */
	bitmap->width = width;
	bitmap->height = height;
	bitmap->channels = channels;
	
	/* Aloca a Array tridimensional
	 * https://mycodelog.com/2010/05/21/array3d/ */
	allElements = malloc( channels * width * height * sizeof( uint8_t ) );
    bitmap->pixel = malloc(channels * sizeof(uint8_t **));
 
    for ( i = 0 ; i < channels ; i++ ) {
        bitmap->pixel[i] = malloc(height * sizeof(uint8_t *));
 
        for ( j = 0 ; j < height ; j++ ){
            bitmap->pixel[i][j] = allElements + (i * width * height ) + (j * width );
        }
    }
    
}

int bitmap_ReadFromFile( bitmap_t * bitmap , const char * path ){
	int32_t i = 0;
	int32_t j = 0;
	int32_t k = 0;
	
	FILE * input_image_ptr;				
	uint32_t input_image_length = 0;
	uint8_t * temp_file_data;	
	
	uint32_t pixel_start_offset = 0;
	int32_t image_width = 0;
	int32_t image_height = 0;
	uint16_t image_bpp = 0;
	
	/* Abre o arquivo */
	input_image_ptr = fopen( path , "rb" );
	
	/* Verifica se o arquivo existe */
	if( input_image_ptr == NULL ){
		return BITMAP_READFROMFILE_ERROR_FILENOTFOUND;
	}
	
	/* Recupera o tamanho do arquivo */
	fseek( input_image_ptr , 0L , SEEK_END );
	input_image_length = ( uint32_t )ftell( input_image_ptr );
	rewind( input_image_ptr );
	
	/* Inicializa o espa�o da mem�ria dinamica que guardar� as informa��es do arquivo */
	temp_file_data = (uint8_t *)malloc( sizeof( uint8_t ) * input_image_length );	
	
	/* Copia o arquivo para um vetor tempor�rio	*/
	fread( temp_file_data , 1/*sizeof(char)*/, input_image_length, input_image_ptr);
	
	/* Fecha o arquivo */
	fclose( input_image_ptr );
	
	/* Verifica se o arquivo recuperado de fato � BMP */
	if( temp_file_data[ BMP_OFFSET_HEADER + 0 ] != 'B' ||
		temp_file_data[ BMP_OFFSET_HEADER + 1 ] != 'M' ){
		return BITMAP_READFROMFILE_ERROR_NOTBITMAP;
	}
	
	/* Recupera o endere�o de start do pixel array */
	pixel_start_offset = *( int32_t *)(&temp_file_data[ BMP_OFFSET_PIXELSTART ] );
	
	/* Recupera a largura do bitmap */
	image_width = *( int32_t *)(&temp_file_data[ BMP_OFFSET_WIDTH ] );
	
	/* Recupera a altura do bitmap */
	image_height = *( int32_t *)(&temp_file_data[ BMP_OFFSET_HEIGHT ] );
	
	/* Recupera o bpp */
	image_bpp = *( uint16_t *)(&temp_file_data[ BMP_OFFSET_BITSPERPIXEL ] );
	
	/* Inicializa o Bitmap */
	bitmap_InitBitmap( bitmap , image_width , image_height , 3 );
	
	/* Converte a informa��o de pixels */
	switch( image_bpp ){
		case 8:
			
		break;
		case 24:
			k = pixel_start_offset;
			for( j = bitmap->height - 1  ; j >= 0  ; j-- ){
				for( i = 0 ; i < bitmap->width ; i++ ){
					/* Coloca os pixels na mem�ria! */
					bitmap->pixel[BLU][j][i] = temp_file_data[ k++ ];
					bitmap->pixel[GRN][j][i] = temp_file_data[ k++ ];
					bitmap->pixel[RED][j][i] = temp_file_data[ k++ ];
				}
				/* Acerta o padding */
				if( (( bitmap->width * 3 ) % 4) != 0 ){
					k += 4 -( ( bitmap->width * 3 ) % 4 );
				}
				
			}
			
			/* Apaga a array alocada dinamicamente */
			free( temp_file_data );
		break;
		default:
			return BITMAP_READFROMFILE_ERROR_BITDEPTHNOTSUPPORTED;
		break;
	}
	
	/* Retorna sucesso */
	return BITMAP_READFROMFILE_SUCCESS;
}



void bitmap_SaveToFile( bitmap_t * bitmap , const char * path ){
	FILE * bitmap_file;
	
	uint32_t size_of_bmp_file = 54 + ( bitmap->width * bitmap->height * bitmap->channels );
	int32_t i = 0;
	int32_t j = 0;
	int32_t k = 0;

		
	bitmap_file = fopen( path , "w+b" );
	if( bitmap_file != NULL ){
		
		/**************
		 * BMP HEADER *
		 *************/
		
		/* BM */
		fputc( 'B' , bitmap_file );
		fputc( 'M' , bitmap_file );
		
		/* Tamanho da Imagem */
		if( bitmap->channels == 3 )
			size_of_bmp_file = 54 + ceil( (double)( bitmap->width * bitmap->channels ) / 4 ) * 4 * bitmap->height;
		else if( bitmap->channels == 1 )
			size_of_bmp_file = 54 + ceil( (double)( bitmap->width * 3 ) / 4 ) * 4 * bitmap->height;
			
			
		fput_u32( size_of_bmp_file , bitmap_file );
		
		/* Placeholder */
		fput_u16( 0 , bitmap_file );
		fput_u16( 0 , bitmap_file );

		/* Offset */
		fput_u32( 54 , bitmap_file );
		
		/**************
		 * DIB HEADER *
		 *************/
		/* Header Size */
		fput_u32( 40 , bitmap_file );		
		
		/* Width */
		fput_u32( bitmap->width , bitmap_file );
		
		/* Height */
		fput_u32( bitmap->height , bitmap_file );
		
		/* Number of color planes */
		fput_u16( 1 , bitmap_file );
		
		/* Number of bits per pixel */
		fput_u16( 24 , bitmap_file );
		
		/* BI_RGB compression */
		fput_u32( 0 , bitmap_file );
		
		/* Size of Raw Bitmapdata */
		fput_u32( 12 , bitmap_file );
		
		/* Print Resolution Horizontal */
		fput_u32( 2835 , bitmap_file );
		
		/* Print Resolution Vertical */
		fput_u32( 2835 , bitmap_file );
		
		/* Number of colors in pallete */
		fput_u32( 0 , bitmap_file );
		
		/* important colors */
		fput_u32( 0 , bitmap_file );
		
		/*********************
		 * Pixel DATA HEADER *
		 ********************/
		for( j = bitmap->height - 1  ; j >= 0  ; j-- ){

			for( i = 0 ; i < bitmap->width  ; i++ ){
				if( bitmap->channels == 3 ){
					/* 24bpp */
					fputc( (uint8_t)( bitmap->pixel[BLU][j][i] ) , bitmap_file );
					fputc( (uint8_t)( bitmap->pixel[GRN][j][i] ) , bitmap_file );
					fputc( (uint8_t)( bitmap->pixel[RED][j][i] ) , bitmap_file );
				} else if( bitmap->channels == 1 ){
					/* Tons de Cinza */
					fputc( (uint8_t)( bitmap->pixel[0][j][i] ) , bitmap_file );
					fputc( (uint8_t)( bitmap->pixel[0][j][i] ) , bitmap_file );
					fputc( (uint8_t)( bitmap->pixel[0][j][i] ) , bitmap_file );
				}
			}
			
			/* PADDING */
			if( (( bitmap->width * 3 ) % 4) != 0 ){
				for( k = 0 ; k < 4 -( ( bitmap->width * 3 ) % 4 ) ; k++ ){
					fputc( 0 , bitmap_file );
				}
			}
			
		}
		
	}
	
	/* Fecha o arquivo */
	fclose( bitmap_file );
}

void bitmap_SaveToFileIndexed( bitmap_t * bitmap , const char * path , uint8_t index_table[][3] , uint32_t index_table_counter ){
	FILE * bitmap_file;
	
	uint32_t size_of_bmp_file = 54 + ( bitmap->width * bitmap->height * bitmap->channels );
	int32_t i = 0;
	int32_t j = 0;
	int32_t k = 0;
	double v = 0;
	double temp = 0;
	int32_t index = 0;
	uint32_t index_table_elements = index_table_counter/3;
	
	printf( "\n!!!Table Elements:%u" , index_table_elements );
		
	bitmap_file = fopen( path , "w+b" );
	if( bitmap_file != NULL ){
		
		/**************
		 * BMP HEADER *
		 *************/
		
		/* BM */
		fputc( 'B' , bitmap_file );
		fputc( 'M' , bitmap_file );
		
		/* Tamanho da Imagem */
		size_of_bmp_file = 54 + ceil( (double)( bitmap->width * bitmap->channels ) / 4 ) * 4 * bitmap->height;			
		fput_u32( size_of_bmp_file , bitmap_file );
		
		/* Placeholder */
		fput_u16( 0 , bitmap_file );
		fput_u16( 0 , bitmap_file );

		/* Offset */
		fput_u32( 54 + index_table_elements*4 , bitmap_file );
		
		/**************
		 * DIB HEADER *
		 *************/
		/* Header Size */
		fput_u32( 40 , bitmap_file );		
		
		/* Width */
		fput_u32( bitmap->width , bitmap_file );
		
		/* Height */
		fput_u32( bitmap->height , bitmap_file );
		
		/* Number of color planes */
		fput_u16( 1 , bitmap_file );
		
		/* Number of bits per pixel */
		fput_u16( 8 , bitmap_file );
		
		/* BI_RGB compression */
		fput_u32( 0 , bitmap_file );
		
		/* Size of Raw Bitmapdata */
		fput_u32( 12 , bitmap_file );
		
		/* Print Resolution Horizontal */
		fput_u32( 2835 , bitmap_file );
		
		/* Print Resolution Vertical */
		fput_u32( 2835 , bitmap_file );
		
		/* Number of colors in pallete */
		fput_u32( index_table_elements , bitmap_file );
		
		/* important colors */
		fput_u32( 0 , bitmap_file );
		
		/**********************
		 * INDEXED COLORS LUT *
		 *********************/
		/* LUT para preto e branco */
		for( j = 0 ; (uint32_t)(j) < index_table_elements ; j++ ){
			fputc( (uint8_t)( index_table[j][BLU]  ) , bitmap_file );
			fputc( (uint8_t)( index_table[j][GRN]  ) , bitmap_file );
			fputc( (uint8_t)( index_table[j][RED]  ) , bitmap_file );
			fputc( (uint8_t)( 0 ) , bitmap_file );
		}
		
		/*********************
		 * Pixel DATA HEADER *
		 ********************/
		for( j = bitmap->height - 1  ; j >= 0  ; j-- ){

			for( i = 0 ; i < bitmap->width  ; i++ ){
				
					/* Verifica entre a tabela e as cores a menor distancia euclidiana*/
					v = 50000.0;
					index = 0;
					for( k = 0 ; (uint32_t)(k) < index_table_elements ; k++ ){
						
						/* Verifica qual dos valores � o mais proximo */
						if( bitmap->channels == 3 ){
							/* 3 Canais */
							temp = rgb_euclidean_distance( bitmap->pixel[RED][j][i],bitmap->pixel[GRN][j][i], bitmap->pixel[BLU][j][i], index_table[k][RED] , index_table[k][GRN] , index_table[k][BLU] );
						} else if( bitmap->channels == 1 ){
							/* 1 Canal */
							temp = rgb_euclidean_distance( bitmap->pixel[0][j][i], bitmap->pixel[0][j][i], bitmap->pixel[0][j][i], index_table[k][RED] , index_table[k][GRN] , index_table[k][BLU] );
						}
						
						if( temp < v ){
							index = k;
							v = temp;
						}
						
					}
							
					/* Aplica o pixel indexado*/
					fputc( (uint8_t)( index ) , bitmap_file );
				}
			
			/* PADDING */
			if( (( bitmap->width ) % 4) != 0 ){
				for( k = 0 ; k < 4 -( ( bitmap->width * 3 ) % 4 ) ; k++ ){
					fputc( 0 , bitmap_file );
				}
			}
			
		}
		
	}
	
	/* Fecha o arquivo */
	fclose( bitmap_file );
}

void	bitmap_Copy( bitmap_t * bitmap_dest , bitmap_t * bitmap_orig , uint32_t width , uint32_t height ,
						uint32_t offset_x , uint32_t offset_y ){
	uint32_t i = 0;
	uint32_t j = 0;
	uint32_t k = 0;
	
	/* Verifica se a largura e altura deve ser o da imagem original ou foi definida no par�metro da fun��o*/
	if( width == 0 )
		width = bitmap_orig->width;
		
	if( height == 0 )
		height = bitmap_orig->height;
		
	/* Inicializa o Bitmap de destino */
	bitmap_InitBitmap( bitmap_dest , width , height , bitmap_orig->channels );
	
	/* Copia a informa��o para a imagem de destino*/
	for( i = 0 ; i < (uint32_t)bitmap_dest->width ; i++ ){
		for( j = 0 ; j < (uint32_t)bitmap_dest->height ; j++ ){
			for( k = 0 ; k < bitmap_dest->channels ; k++ ){
				bitmap_dest->pixel[k][j][i] = bitmap_orig->pixel[k][j+offset_y][i+offset_x];
			}
		}
	}
	
	
}

