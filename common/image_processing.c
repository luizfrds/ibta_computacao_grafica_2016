/****************************************************************************
 *	Copyright (C) 2016 by Luiz S�				                            *
 *                                                                          *
 *	Esse arquivo � parte das listas de Exerc�cio de Computa��o Gr�fica      *
 *	ministradas no 2� Semestre de 2016 pelo prof� S�rgio Penedo				*
 *                                                                          *
 ****************************************************************************/

/**
 * @file image_processing.c
 * @author Luiz S�
 * @date 23 09 2016
 * @brief Fun��es respons�veis por processamento de imagens
 *
 * Fun��es gen�ricas de processamento de imagens
 * 
 * @see https://en.wikipedia.org/wiki/BMP_file_format
 */
 
#include "image_processing.h"
#include "helpers.h"
#include "file_bitmap.h"

uint8_t index_table_gray[256][3] = {{0,0,0},{1,1,1},{2,2,2},{3,3,3},{4,4,4},{5,5,5},{6,6,6},{7,7,7},{8,8,8},{9,9,9},{10,10,10},{11,11,11},{12,12,12},{13,13,13},{14,14,14},{15,15,15},{16,16,16},{17,17,17},{18,18,18},{19,19,19},{20,20,20},{21,21,21},{22,22,22},{23,23,23},{24,24,24},{25,25,25},{26,26,26},{27,27,27},{28,28,28},{29,29,29},{30,30,30},{31,31,31},{32,32,32},{33,33,33},{34,34,34},{35,35,35},{36,36,36},{37,37,37},{38,38,38},{39,39,39},{40,40,40},{41,41,41},{42,42,42},{43,43,43},{44,44,44},{45,45,45},{46,46,46},{47,47,47},{48,48,48},{49,49,49},{50,50,50},{51,51,51},{52,52,52},{53,53,53},{54,54,54},{55,55,55},{56,56,56},{57,57,57},{58,58,58},{59,59,59},{60,60,60},{61,61,61},{62,62,62},{63,63,63},{64,64,64},{65,65,65},{66,66,66},{67,67,67},{68,68,68},{69,69,69},{70,70,70},{71,71,71},{72,72,72},{73,73,73},{74,74,74},{75,75,75},{76,76,76},{77,77,77},{78,78,78},{79,79,79},{80,80,80},{81,81,81},{82,82,82},{83,83,83},{84,84,84},{85,85,85},{86,86,86},{87,87,87},{88,88,88},{89,89,89},{90,90,90},{91,91,91},{92,92,92},{93,93,93},{94,94,94},{95,95,95},{96,96,96},{97,97,97},{98,98,98},{99,99,99},{100,100,100},{101,101,101},{102,102,102},{103,103,103},{104,104,104},{105,105,105},{106,106,106},{107,107,107},{108,108,108},{109,109,109},{110,110,110},{111,111,111},{112,112,112},{113,113,113},{114,114,114},{115,115,115},{116,116,116},{117,117,117},{118,118,118},{119,119,119},{120,120,120},{121,121,121},{122,122,122},{123,123,123},{124,124,124},{125,125,125},{126,126,126},{127,127,127},{128,128,128},{129,129,129},{130,130,130},{131,131,131},{132,132,132},{133,133,133},{134,134,134},{135,135,135},{136,136,136},{137,137,137},{138,138,138},{139,139,139},{140,140,140},{141,141,141},{142,142,142},{143,143,143},{144,144,144},{145,145,145},{146,146,146},{147,147,147},{148,148,148},{149,149,149},{150,150,150},{151,151,151},{152,152,152},{153,153,153},{154,154,154},{155,155,155},{156,156,156},{157,157,157},{158,158,158},{159,159,159},{160,160,160},{161,161,161},{162,162,162},{163,163,163},{164,164,164},{165,165,165},{166,166,166},{167,167,167},{168,168,168},{169,169,169},{170,170,170},{171,171,171},{172,172,172},{173,173,173},{174,174,174},{175,175,175},{176,176,176},{177,177,177},{178,178,178},{179,179,179},{180,180,180},{181,181,181},{182,182,182},{183,183,183},{184,184,184},{185,185,185},{186,186,186},{187,187,187},{188,188,188},{189,189,189},{190,190,190},{191,191,191},{192,192,192},{193,193,193},{194,194,194},{195,195,195},{196,196,196},{197,197,197},{198,198,198},{199,199,199},{200,200,200},{201,201,201},{202,202,202},{203,203,203},{204,204,204},{205,205,205},{206,206,206},{207,207,207},{208,208,208},{209,209,209},{210,210,210},{211,211,211},{212,212,212},{213,213,213},{214,214,214},{215,215,215},{216,216,216},{217,217,217},{218,218,218},{219,219,219},{220,220,220},{221,221,221},{222,222,222},{223,223,223},{224,224,224},{225,225,225},{226,226,226},{227,227,227},{228,228,228},{229,229,229},{230,230,230},{231,231,231},{232,232,232},{233,233,233},{234,234,234},{235,235,235},{236,236,236},{237,237,237},{238,238,238},{239,239,239},{240,240,240},{241,241,241},{242,242,242},{243,243,243},{244,244,244},{245,245,245},{246,246,246},{247,247,247},{248,248,248},{249,249,249},{250,250,250},{251,251,251},{252,252,252},{253,253,253},{254,254,254},{255,255,255}};
uint8_t index_table_blackandwhite[2][3] = {{0,0,0},{255,255,255}};

double rgb_euclidean_distance( double r0, double g0, double b0, double r1, double g1, double b1 ){
	return sqrt( (r0-r1)*(r0-r1) + (g0-g1)*(g0-g1) + (b0-b1)*(b0-b1) );
}

void draw_Rectangle( bitmap_t * bmp , uint32_t border_width , uint32_t x , uint32_t y , uint32_t width , uint32_t height ){
	int32_t x_min = 0;
	int32_t x_max = 0;
	int32_t y_min = 0;
	int32_t y_max = 0;
	int32_t i = 0;
	int32_t j = 0;
	
	/* Garante que as bordas do retangulo estejam dentro dos limites do BMP */
	x_min = max( (int32_t)( x - border_width ) , 0 );
	x_max = min( (int32_t)( x + width + border_width ) , bmp->width );
	
	y_min = max( (int32_t)( y - border_width ) , 0 );
	y_max = min( (int32_t)( y + height + border_width ) , bmp->height );
	
	/* Desenha no bitmap de entrada as bordas do retangulo*/
	for( j = y_min ; j < y_max ; j++ ){
		for( i = x_min ; i < x_max ; i++ ){
			if( ( i - (int32_t)( x - border_width ) ) == (int32_t)border_width && 
				( ( j - (int32_t)( y - border_width ) ) >= (int32_t)border_width && ( j - (int32_t)( y - border_width ) ) < (int32_t)(border_width + height ) ) ) {
				i += width ;
				continue;
			}
			bmp->pixel[RED][j][i] = 0;
			bmp->pixel[GRN][j][i] = 0;
			bmp->pixel[BLU][j][i] = 0;
		}
	}
}



void processing_RGBToLuma( bitmap_t * bmp_output , bitmap_t * bmp_input ){
	int32_t i = 0;
	int32_t j = 0;
	
	bitmap_InitBitmap( bmp_output , bmp_input->width , bmp_input->height , 1 );
	
	for( j = 0 ; j < bmp_output->height ; j++ ) {
		for( i = 0 ; i < bmp_output->width ; i++ ){
			bmp_output->pixel[GRAY][j][i] = (uint8_t)( 0.30*(double)(bmp_input->pixel[RED][j][i]) + 0.59*(double)(bmp_input->pixel[GRN][j][i]) + 0.11*(double)(bmp_input->pixel[BLU][j][i]) );
		}
	}

}

void processing_NormalizeChannel( bitmap_t * bmp , double percentual ){
	int32_t i = 0;
	int32_t j = 0;
	uint32_t histograma[256];
	
	uint32_t counter = 0;
	uint32_t compress = (uint32_t)( percentual * ( bmp->width*bmp->height ) );
	double v_min = 0.0;
	double v_max = 255.0;	
	
	/* Reseta o histograma*/
	for( i = 0 ; i < 256 ; i++ ) {
		histograma[i] = 0;
	}
	
	/* Recupera o histograma */
	for( j = 0 ; j < bmp->height ; j++ ) {
		for( i = 0 ; i < bmp->height ; i++ ){
			histograma[ bmp->pixel[GRAY][j][i] ]++;
		}
	}

	/* Define o valor m�nimo */
	counter = 0;
	for( i = 0 ; i < 256 ; i++ ){
		counter += histograma[i];
		if( counter >= compress ){
			v_min = i;
			break;
		}
	}
	
	/* Define o valor m�ximo */
	counter = 0;
	for( i = 256 ; i >= 0 ; i-- ){
		counter += histograma[i];
		if( counter >= compress ){
			v_max = i;
			break;
		}
	}
	
	/* Estica o histograma */
	for( j = 0 ; j < bmp->height ; j++ ) {
		for( i = 0 ; i < bmp->height ; i++ ){
			bmp->pixel[GRAY][j][i] = max( bmp->pixel[GRAY][j][i] - v_min , 0 );
			bmp->pixel[GRAY][j][i] = min( 255 , bmp->pixel[GRAY][j][i] * (255.0/( v_max - v_min ) ) );
		}
	}
	
}

void processing_GrayToHalftone( bitmap_t * bmp_output , bitmap_t * bmp_input ){
	int32_t i = 0;
	int32_t j = 0;
	
	uint8_t ht_intensity = 0;
	
	uint8_t halftone_table[5][2][2] = 
								{
									{ 	
										{  0  ,  0  },
										{  0  ,  0  } 	
									},
									{ 	
										{ 255 ,  0  },
										{  0  ,  0  } 	
									},
									{ 	
										{ 255 ,  0  },
										{  0  , 255 } 	
									},
									{ 	
										{ 255 , 255 },
										{  0  , 255 } 	
									},
									{ 	
										{ 255 , 255 },
										{ 255 , 255 } 
									},
								};

	
	bitmap_InitBitmap( bmp_output , bmp_input->width*2 , bmp_input->height*2 , 1 );
	
	for( j = 0 ; j < bmp_output->height ; j+=2 ) {
		for( i = 0 ; i < bmp_output->width ; i+=2 ){
			
			ht_intensity = bmp_input->pixel[GRAY][j/2][i/2] / 63.75;
			
			bmp_output->pixel[GRAY][j][i] 		= halftone_table[ht_intensity][0][0];
			bmp_output->pixel[GRAY][j][i+1] 	= halftone_table[ht_intensity][0][1];
			bmp_output->pixel[GRAY][j+1][i] 	= halftone_table[ht_intensity][1][0];
			bmp_output->pixel[GRAY][j+1][i+1] 	= halftone_table[ht_intensity][1][1];
		}
	}
}

void processing_ApplyConvolutionMask( bitmap_t * bmp_output , bitmap_t * bmp_input , const int32_t conv_mask[3][3] ){
	int32_t i = 0;
	int32_t j = 0;
	int32_t k = 0;
	
	int32_t	p = 0;
	int32_t div_factor = 0;
	
	bitmap_InitBitmap( bmp_output , bmp_input->width , bmp_input->height , bmp_input->channels );
	
	/* Define o fator de Divis�o */
	for( i = 0 ; i < 3 ; i++ ){
		for( j = 0 ; j < 3 ; j++ ){
			div_factor += conv_mask[i][j];
		}
	}
	
	/* Aplica a m�scara */
	for( k = 0 ; k < bmp_input->channels ; k++ ) {			
		for( j = 1 ; j < bmp_input->height - 1 ; j++ ) {		
			for( i = 1 ; i < bmp_input->width - 1 ; i++ ){					
				p = ( bmp_input->pixel[k][j-1][i-1] * conv_mask[2][2] ) + ( bmp_input->pixel[k][j-1][i] * conv_mask[2][1] ) + ( bmp_input->pixel[k][j-1][i+1] * conv_mask[2][0] ) + 
					( bmp_input->pixel[k][j+0][i-1] * conv_mask[1][2] ) + ( bmp_input->pixel[k][j+0][i] * conv_mask[1][1] ) + ( bmp_input->pixel[k][j+0][i+1] * conv_mask[1][0] ) + 
					( bmp_input->pixel[k][j+1][i-1] * conv_mask[0][2] ) + ( bmp_input->pixel[k][j+1][i] * conv_mask[0][1] ) + ( bmp_input->pixel[k][j+1][i+1] * conv_mask[0][0] );
				
				if( div_factor != 0 )
					p /= div_factor;
				
				p = min( 255 , p )	;
				p = max( 0 , p );
				bmp_output->pixel[k][j][i] = (uint8_t)( p );
			} /* Channel */
		} /* Vertical */
	} /* Horizontal */
}
