/****************************************************************************
 *	Instituto Brasileiro de Tecnologia Avan�ada - 2016 S2					*
 *  Computa��o Gr�fica - Prof� Dr� S�rgio Penedo							*
 *																			*
 *  Renata Silva															*
 *  Luiz S�																	*
 *                                                                          *
 *	Lista 1 - Exerc�cio 1													*
 *  																		*
 * 	Localizar uma subimagem dentro de uma imagem e exibir um ret�ngulo de 	*
 *	a subimagem foi extra�da												*
 *                                                                          *
 *	Instru��es:																*
 *	- Todas as imagens de entrada tem de ser .BMP 24bpp						*																			*
 *	- Para testar imagens diferentes basta substituir os endere�os na linha	*
 *	  65 e 66 deste arquivo													*
 *																			*
 ****************************************************************************/ 
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "../common/helpers.h"
#include "../common/image_processing.h"
#include "../common/file_bitmap.h"

/*
 * @brief Gera um bitmap de erro que � a taxa de erro pela m�dia da distancia euclidiana pela �rea do bitmap de entrada
 *
 * O algor�timo faz uma compara��o de janela m�vel nos eixos horizontais e verticais, comparando todos os pixels que se
 * intereseccionam na opera��o, depois � c�lculada a m�dia do erro pela �rea do bitmap parcial
 * 
 * @param main_bitmap 		Bitmap Original de Refer�ncia
 * @param partial_bitmap 	Bitmap Parcial
 * @param bitmap_error 		Offset vertical em rela��o ao canto superior esquerdo do bitmap
*/
void generate_bitmap_error( bitmap_t * main_bitmap , bitmap_t * partial_bitmap, bitmap_t * bitmap_error );

/*
 * @brief Serve apenas para printar no terminal o sucesso ou n�o do carregamento da imagem
 * 
 * @param file_success_status 		Estado de sucesso do carregamento de arquivo
*/
void bitmap_file_success_printf( int32_t file_success_status );

/**
 * @brief Loop principal
 *
 * Loop principal do programa
 */
int main( /*int argc, char *argv[] */) {
	int32_t file_a_success = 0;
	int32_t file_b_success = 0;
	
	int32_t i = 0;
	int32_t j = 0;
	
	bitmap_t bitmap_input;
	bitmap_t bitmap_part;
	
	bitmap_t bitmap_error;

	/* ARQUIVOS DE ENTRADA */
	file_a_success = bitmap_ReadFromFile( &bitmap_input , "../bitmaps/lenna.bmp" );
	file_b_success = bitmap_ReadFromFile( &bitmap_part , "../bitmaps/lenna_part.bmp" );
	
	/* Verifica se o arquivo de entrada da imagem de refer�ncia � v�lido*/
	printf("Arquivo de Referencia:\n");
	bitmap_file_success_printf( file_a_success );
	
	/* Verifica se o arquivo de entrada da imagem parcial � v�lida*/
	printf("\nArquivo da imagem parcial:\n");
	bitmap_file_success_printf( file_b_success );
	
	printf("\nAguarde ");
	
	/* Caso as duas imagens sejam v�lidas*/
	if( file_a_success ==  BITMAP_READFROMFILE_SUCCESS && file_b_success ==  BITMAP_READFROMFILE_SUCCESS ){
		
		/* Gera o bitmap de Erro */
		generate_bitmap_error( &bitmap_input , &bitmap_part , &bitmap_error );
		printf(".");
		
		/* Salva o bitmap de erro*/
		bitmap_SaveToFileIndexed( &bitmap_error, "output_error_map.bmp" , index_table_gray , sizeof( index_table_gray ) );
		printf(".");
		
		/* Analisa o bitmap de erro e exibe as �reas  */
		for( j = 0 ; j < bitmap_error.height ; j++ ){
			for( i = 0 ; i < bitmap_error.width ; i++ ){
				if( bitmap_error.pixel[GRAY][j][i] <= 5 ){
					draw_Rectangle( &bitmap_input, 5, i, j, bitmap_part.width, bitmap_part.height );
				}
			}
		}
		printf(".");
		
		/* Salva o bitmap com as �reas de semelhan�a demarcadas*/
		bitmap_SaveToFile( &bitmap_input, "output.bmp" );		
		printf(".");
	}
		
		
	printf(" Fim!");
	/* Libera os bitmaps da mem�ria*/
	bitmap_FreeBitmap( &bitmap_input );
	bitmap_FreeBitmap( &bitmap_part );
	bitmap_FreeBitmap( &bitmap_error );
	
	return 0;
}

void generate_bitmap_error( bitmap_t * main_bitmap , bitmap_t * partial_bitmap, bitmap_t * bitmap_error ){
	int32_t i = 0;
	int32_t j = 0;
	
	int32_t k = 0;
	int32_t l = 0;
	
	double integral_error = 0;
	
	int32_t search_width = 0;
	int32_t search_height = 0;
	
	bitmap_t bitmap_temp;
	
	search_width = main_bitmap->width - partial_bitmap->width;
	search_height = main_bitmap->height - partial_bitmap->height;
	
	/* Inicializa o Bitmap de Erro*/
	bitmap_InitBitmap( bitmap_error , search_width , search_height , 1 );
	
	/* Roda o Offset a diferen�a */
	for( i = 0 ; i < search_height ; i++ ){
		for( j = 0 ; j < search_width ; j++ ){
			/* Copia a janela do bitmap original para uma vari�vel tempor�ria*/
			bitmap_Copy( &bitmap_temp , main_bitmap , partial_bitmap->width , partial_bitmap->height , j , i );
			
			/* Calcula a diferen�a euclidiana para cada um dos pixels*/
			integral_error = 0;
			for( k = 0 ; k <  bitmap_temp.width ; k++ ){
				for( l = 0 ; l < bitmap_temp.height ; l++ ){
					integral_error += rgb_euclidean_distance( bitmap_temp.pixel[RED][l][k], bitmap_temp.pixel[GRN][l][k], bitmap_temp.pixel[BLU][l][k] , partial_bitmap->pixel[RED][l][k] , partial_bitmap->pixel[GRN][l][k] , partial_bitmap->pixel[BLU][l][k] ) / 441.67295593;
				}
			}
			
			/* faz o c�lculo do erro para o offset atual */
			bitmap_error->pixel[GRAY][i][j] = ( integral_error / ( (double)bitmap_temp.width * (double)bitmap_temp.height ) ) * 255.0;
			
			/*libera o bitmap tempor�rio da mem�ria*/
			bitmap_FreeBitmap( &bitmap_temp );
		}
	}
}

void bitmap_file_success_printf( int32_t file_success_status ){
	switch( file_success_status ){
		case BITMAP_READFROMFILE_SUCCESS:
			printf("Sucesso!\n");
		break;
		
		case BITMAP_READFROMFILE_ERROR_FILENOTFOUND:
			printf("ERRO: Arquivo nao localizado!\n");
		break;
		
		case BITMAP_READFROMFILE_ERROR_NOTBITMAP:
			printf("ERRO: O arquivo fornecido nao e um BITMAP!\n");
		break;
		case BITMAP_READFROMFILE_ERROR_BITDEPTHNOTSUPPORTED:
			printf("ERRO: O BitDepth da imagem n�o � suportado!\n");
		break;
		default:
			printf("ERRO: ???\n");
		break;
	}
}
